# IAM deployment for %%FQDN%%

Check the `kustomization.yaml` for deployment configuration options.

## Check that setup work as expected

```console
kubectl apply -oyaml --dry-run=client -k .
```

## Apply configuration

```console
kubectl apply -k .
```