# IAM K8S basic configuration and utils

## Requirements

- openssl
- kustomize

## How to deploy a new IAM instance on our K8S cluster

1. Obtain a database schema and credentials for the new IAM instance.
2. Create a namespace for your new IAM instance. Annotate the namespace name
3. Create a new iam deployment with the script utils/create-deployment.sh, as
   follows:
   ```console
   NAMESPACE=example FQDN=iam-demo.example VOMS_FQDN=voms-demo.example sh utils/create-deployment.sh
   ```
4. Follow the instructions in the README.md file in the deployment directory
