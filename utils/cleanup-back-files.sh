#!/bin/bash
set -e

echo "Removing backup files..."

find ./kustomize/overlays -name '*.bak' -exec rm -vf {} \;
