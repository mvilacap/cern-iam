#!/bin/bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

OVERLAYS_DIR=${DIR}/../kustomize/overlays
TEMPLATE_DIR=${DIR}/../kustomize/.templates

if [ -z "${NAMESPACE}" ]; then
  echo "Please set the NAMESPACE env variable (with the namespace name of the IAM deployment)!"
  exit 1
fi

if [ -z "${FQDN}" ]; then
  echo "Please set the FQDN env variable (with the fqdn of the IAM deployment)!"
  exit 1
fi

VOMS_FQDN=${VOMS_FQDN:-voms.example}

TARGET_DIR=${OVERLAYS_DIR}/${FQDN}

if [ -d ${TARGET_DIR} ]; then
  echo "Deployment directory ${TARGET_DIR} already exists"
  exit 1
fi

mkdir ${TARGET_DIR}
cp -r ${TEMPLATE_DIR}/* ${TARGET_DIR}

KEY_ID=$(echo ${FQDN}| cut -d"." -f1)

## Change the README
sed -e "s#%%FQDN%%#${FQDN}#" ${TEMPLATE_DIR}/README.md > ${TARGET_DIR}/README.md

## Edit deployment template
sed -e "s#%%FQDN%%#${FQDN}#" ${TEMPLATE_DIR}/deployment-name.patch.yaml > ${TARGET_DIR}/deployment-name.patch.yaml

## Edit login-service env variables
sed -e "s#%%FQDN%%#${FQDN}#" -e "s#%%KEY_ID%%#${KEY_ID}#" ${TEMPLATE_DIR}/login-service.env > ${TARGET_DIR}/login-service.env

## Edit namespace name in kustomization
sed -e "s#%%NAMESPACE%%#${NAMESPACE}#" ${TEMPLATE_DIR}/kustomization.yaml > ${TARGET_DIR}/kustomization.yaml

## SAML keystore secret
sed -e "s#%%KEY_ID%%#${KEY_ID}#" ${TEMPLATE_DIR}/saml/saml-keystore-secret.env > ${TARGET_DIR}/saml/saml-keystore-secret.env

## Generate JSON web keys
sh ${DIR}/jwk/generate-jwks.sh > ${TARGET_DIR}/json-keystore.jwks

## Generate SAML keystore
sh ${DIR}/saml/generate-self-signed-cert.sh ${FQDN}
cp ${DIR}/saml/certs/${FQDN}.jks ${TARGET_DIR}/saml/saml-keystore.jks

## VOMS stuff
sed -e "s#%%VOMS_FQDN%%#${VOMS_FQDN}#" ${TEMPLATE_DIR}/voms-aa/kustomization.yaml > ${TARGET_DIR}/voms-aa/kustomization.yaml

## LSC config
sed -e "s#%%VOMS_FQDN%%#${VOMS_FQDN}#" ${TEMPLATE_DIR}/voms-aa/vomsdir/template.lsc > ${TARGET_DIR}/voms-aa/vomsdir/${VOMS_FQDN}.lsc

## NGINX config
sed -e "s#%%VOMS_FQDN%%#${VOMS_FQDN}#" ${TEMPLATE_DIR}/voms-aa/nginx/conf.d/voms-aa.conf > ${TARGET_DIR}/voms-aa/nginx/conf.d/voms-aa.conf

## Deployment name patch
sed -e "s#%%VOMS_FQDN%%#${VOMS_FQDN}#" ${TEMPLATE_DIR}/voms-aa/voms-aa.patch.yaml > ${TARGET_DIR}/voms-aa/voms-aa.patch.yaml

## Database credentials handling (if set)

echo "IAM deployment for ${FQDN} generated in: $(realpath ${TARGET_DIR})"
