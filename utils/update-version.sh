#!/bin/bash
set -e

FROM_VERSION=${FROM_VERSION:-$1}
TO_VERSION=${TO_VERSION:-$2}

if [ -z "${FROM_VERSION}" ]; then
  echo "FROM_VERSION not set"
  exit 1
fi

if [ -z "${TO_VERSION}" ]; then
  echo "TO_VERSION not set"
  exit 1
fi

echo "Will update deployments from ${FROM_VERSION} to ${TO_VERSION}"
echo "Deployment that match:"

git grep ${FROM_VERSION} -- ./kustomize/overlays

find ./kustomize/overlays -name kustomization.yaml -exec sed -i.bak "s/${FROM_VERSION}/${TO_VERSION}/" {} \;

