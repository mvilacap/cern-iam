#!/bin/bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ $# -lt 1 ]; then
    echo "Please provide the FQDN for this certificate"
    echo "./generate-self-signed-cert.sh [FQDN]"
    exit 1
fi

NAME="$1"
KEY_NAME=$(echo ${NAME}| cut -d"." -f1)
DAYS=3000
PASSWORD=${KS_PASSWORD:-pass123}

CERT_DIR="certs"

FNAME=${DIR}/${CERT_DIR}/${NAME}

ssl_conf=$(mktemp)

rm -f ${FNAME}.*

sed -e "s#%%FQDN%%#${NAME}#" ${DIR}/conf/ssl.conf.template > ${ssl_conf}

openssl req -newkey rsa:2048 -nodes -x509 -days ${DAYS} \
    -keyout ${FNAME}.key.pem \
    -out ${FNAME}.cert.pem \
    -config ${ssl_conf}

openssl pkcs12 -export -inkey ${FNAME}.key.pem \
    -name ${KEY_NAME} \
    -in ${FNAME}.cert.pem \
    -out ${FNAME}.p12 \
    -passout "pass:${PASSWORD}"

keytool -importkeystore -destkeystore ${FNAME}.jks \
    -srckeystore ${FNAME}.p12 \
    -srcstoretype PKCS12  \
    -srcstorepass ${PASSWORD} \
    -deststorepass ${PASSWORD} \
    -noprompt

keytool -import -alias idem-signer-2024 -keystore ${FNAME}.jks -file ${DIR}/conf/idem-signer-20241118.pem -noprompt -storepass ${PASSWORD}
keytool -import -alias infn_idp_aai -trustcacerts -file ${DIR}/conf/idp.infn.it.der -noprompt -storepass ${PASSWORD} -keystore ${FNAME}.jks

keytool -list -keystore ${FNAME}.jks -storepass ${PASSWORD}

echo "Certificate ${FNAME}.jks generated in ${DIR}/${CERT_DIR}"
echo "key name: ${KEY_NAME}"
echo "password: ${PASSWORD}"

